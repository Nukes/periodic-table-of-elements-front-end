# Stage 0, based on Node.js, to build and compile Angular
FROM node:8.11.1 as builder

# Copy the package.json file first in order to cache the modules
COPY ./package.json /app/package.json

WORKDIR /app
# Install npm dependencies
RUN npm install

# Copy the project
COPY . /app

# Building the Angular app
RUN npm run build


# EXPOSE 4200
# CMD npm run start


# Stage 1, based on Nginx, to have only the compiled app
FROM nginx

# Configure for angular fallback routes
# COPY nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=builder /app/dist /usr/share/nginx/html
COPY ./nginx-development.conf /etc/nginx/conf.d/default.conf
