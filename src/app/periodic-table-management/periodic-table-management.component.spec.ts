import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodicTableManagementComponent } from './periodic-table-management.component';

describe('PeriodicTableManagementComponent', () => {
  let component: PeriodicTableManagementComponent;
  let fixture: ComponentFixture<PeriodicTableManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodicTableManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodicTableManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
