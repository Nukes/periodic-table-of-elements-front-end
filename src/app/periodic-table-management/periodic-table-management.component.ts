import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import {NgForm} from '@angular/forms';
import { FormsModule }    from '@angular/forms'; // Importing forms module to this file



@Component({
  selector: 'app-periodic-table-management',
  templateUrl: './periodic-table-management.component.html',
  styleUrls: ['./periodic-table-management.component.css']
})
export class PeriodicTableManagementComponent implements OnInit {

  fileToUpload: File = null;
  url = "http://localhost:3000/api/containers/container1/upload?filename=";
  yourFileName = {
    value : 'default',
};
  public uploader: FileUploader = new FileUploader({url:"http://localhost:3000/api/containers/container1/upload?filename="+this.yourFileName.value+".jpg"});


  constructor() {


  }


  // uploader: FileUploader = new FileUploader({ url: "http://localhost:3000/api/containers/container1/upload?filename="+this.yourFileName.value+".jpg", removeAfterUpload: false, autoUpload: true });

  beforeUpload(value): void {
    this.yourFileName.value = value;
    console.log(this.yourFileName.value);

  };

  upload(value){
    this.yourFileName.value = value;
    console.log(this.yourFileName.value);
    this.uploader.uploadAll();




  };

  ngOnInit() {

    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false;
      file.url = this.url + file.file.name;
      // file.url = this.url + this.yourFileName.value;

    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
      alert('File uploaded successfully');
    };


  }

}
