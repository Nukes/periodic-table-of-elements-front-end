import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PeriodicTableManagementComponent } from './periodic-table-management/periodic-table-management.component';
import { PeriodicTableDashboardComponent } from './periodic-table-dashboard/periodic-table-dashboard.component';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { FileUploadModule } from 'ng2-file-upload';

const appRoutes: Routes = [
  { path: 'periodic-table-management', component: PeriodicTableManagementComponent },
  { path: 'periodic-table-dashboard',  component: PeriodicTableDashboardComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PeriodicTableManagementComponent,
    PeriodicTableDashboardComponent,
  ],
  imports: [
    BrowserModule,
    AngularFileUploaderModule,
    FileUploadModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
