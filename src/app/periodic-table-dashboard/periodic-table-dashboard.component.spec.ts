import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodicTableDashboardComponent } from './periodic-table-dashboard.component';

describe('PeriodicTableDashboardComponent', () => {
  let component: PeriodicTableDashboardComponent;
  let fixture: ComponentFixture<PeriodicTableDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodicTableDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodicTableDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
