import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-periodic-table-dashboard',
  templateUrl: './periodic-table-dashboard.component.html',
  styleUrls: ['./periodic-table-dashboard.component.less']
})
export class PeriodicTableDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
