import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'the dynamic periodic elements table';
  dashboard :boolean;
  management:boolean;
  ngOnInit() {
    this.dashboard = true;
    this.management = false;
  }

  switching(value1, value2){
    this.dashboard= value2;
    this.management = value1;
    console.log(this.management, this.dashboard);
  };
}
